/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oe;

import hu.oe.javaee.adatok.LoginException;
import hu.oe.javaee.adatok.RegistrationException;
import hu.oe.javaee.adatok.User;
import hu.oe.javaee.adatok.UserRepository;
import javax.inject.Inject;
import javax.inject.Singleton;

/**
 *
 * @author javaee
 */
@Singleton
public class UserService {
 
    @Inject
    UserRepository userRepo;
    
      
    public void Registration(String pName, String pPassword) throws RegistrationException{
        try{//userRepo.getByName(pName);
            userRepo.add(new User(pName,pPassword, false));
        }
        catch(Exception e){throw new RegistrationException();}        
         
    }
    
    public User Login(String pName, String pPassword) throws LoginException{
       try{
           User user = userRepo.getByNamePassword(pName, pPassword);
           return user;
        }
        catch(Exception e){throw new LoginException();}   
    }
}
