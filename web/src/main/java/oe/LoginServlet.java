package oe;

import hu.oe.javaee.adatok.BuildingRepository;
import hu.oe.javaee.adatok.LoginException;
import hu.oe.javaee.adatok.RegistrationException;
import hu.oe.javaee.adatok.SpeciesRepository;
import hu.oe.javaee.adatok.UserRepository;
import java.io.IOException;
import java.io.PrintWriter;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author javaee
 */
@WebServlet(name ="LoinServlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {
    
    @Inject
    UserService userService;
    @Inject
    SpeciesRepository speciesRepository;

    
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String name = request.getParameter("name");
        String password = request.getParameter("password");
        
        try{
            request.getSession().setAttribute("user", userService.Login(name, password)); 
            request.setAttribute("species", speciesRepository.getSpecies());
            request.setAttribute("user", userService.Login(name, password)); 
            getServletContext().getRequestDispatcher("/user.jsp").include(request, response);
        }
        catch(LoginException e)
        {response.getWriter().print("Nem jo login");}
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
