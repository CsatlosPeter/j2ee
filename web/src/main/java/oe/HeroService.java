package oe;


import hu.oe.javaee.adatok.HeroRepository;
import hu.oe.javaee.adatok.Hero;
import hu.oe.javaee.adatok.Hybrid;
import hu.oe.javaee.adatok.UserRepository;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author javaee
 */
@Stateless
public class HeroService {
    
    @Inject
    HeroRepository heroRepository;
    
    
    public Hero add(Hero pHero)
    {
        byte sum =0;
        if (pHero !=null && pHero.getHybrid() !=null) {
            
        for(Hybrid h: pHero.getHybrid())
            sum+=h.getPercent();
        }
        if (sum !=100) {
            throw new ValidateHeroException();
        }
        if(heroRepository.getByNameUser(pHero.getName(), pHero.getUser()).size()>0)
            throw new ValidateHeroException();
        heroRepository.add(pHero);
        return pHero;
    }
    
    public List<Hero> getHeroes() {
        return heroRepository.getHeroes();
    }
    
}
