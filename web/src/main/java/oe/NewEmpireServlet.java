package oe;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import hu.oe.javaee.adatok.BuildingRepository;
import hu.oe.javaee.adatok.Empire;
import hu.oe.javaee.adatok.NaturalAsset;
import hu.oe.javaee.adatok.NaturalAssetRepository;
import hu.oe.javaee.adatok.People;
import hu.oe.javaee.adatok.Population;
import hu.oe.javaee.adatok.SpeciesRepository;
import hu.oe.javaee.adatok.Stock;
import hu.oe.javaee.adatok.User;
import java.io.IOException;
import java.io.PrintWriter;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author javaee
 */
@WebServlet(urlPatterns = {"/NewEmpireServlet"})
public class NewEmpireServlet extends HttpServlet {
    @Inject
    NaturalAssetRepository naturalAssetRepository;
    
    @Inject
    BuildingRepository buildingRepository;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet NewEmpireServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet NewEmpireServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        User sess = ((User)request.getSession().getAttribute("user"));
        
        Empire empire = new Empire(request.getParameter("ename"), request.getParameter("desc"), 1);
        
        for(NaturalAsset na: naturalAssetRepository.getNaturalAssets())
        {
            NaturalAsset nna = new NaturalAsset(na.getName(), na.getDescription());
            empire.getProduce().add(new Stock(nna, 1 + (long) (Math.random()*(100-1))));
            
            empire.getWarehouse().add(new Stock(nna, 15));
            
        }
        empire.getPopulation().add(new Population(new People("First Person", "The first percon"), 1));
        
        sess.getEmpire().add(empire);
        //response.getWriter().println(".....Birodalom hozzaadva.....");
        //response.getWriter().println(sess.getEmpire().get(0).getProduce().get(0).getAsset().getName() + sess.getEmpire().get(0).getProduce().get(0).getQuantity());
        
        //response.getWriter().println(sess.getEmpire().get(0).getProduce().get(1).getAsset().getName() + sess.getEmpire().get(0).getProduce().get(1).getQuantity());
        
        //response.getWriter().println(sess.getEmpire().get(0).getProduce().get(2).getAsset().getName() + sess.getEmpire().get(0).getProduce().get(2).getQuantity());
        request.setAttribute("buildings", buildingRepository.getBuildings());
        
        
        //if (request.getParameter("ename").equals("")) {
            request.setAttribute("currentEmpire", empire);
        //}
        //else{
        //request.setAttribute("currentEmpire", empire);
        //}
        
        
        getServletContext().getRequestDispatcher("/building.jsp").include(request, response);
        
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
