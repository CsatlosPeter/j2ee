package oe;


import hu.oe.javaee.adatok.BuildingRepository;
import hu.oe.javaee.adatok.HeroRepository;
import hu.oe.javaee.adatok.NaturalAssetRepository;
import hu.oe.javaee.adatok.Species;
import hu.oe.javaee.adatok.SpeciesRepository;
import hu.oe.javaee.adatok.User;
import hu.oe.javaee.adatok.UserRepository;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author javaee
 */
@ApplicationScoped
public class AppLicationConfiguration {
    
    @Produces
    @ApplicationScoped
    public UserRepository createUserRepository(){
        UserRepository ur = new UserRepository();
    try{
        //ur.add(new User("a", "a", false));
        //ur.add(new User("b", "n", false));
    }
    catch(Exception e) {e.printStackTrace();}
    return ur;
    }
    
    @Produces
    @ApplicationScoped
    public SpeciesRepository createSpeciesRepository(){
        SpeciesRepository ur = new SpeciesRepository();
    try{
        //ur.add(new Species("Ember", "okos"));
        //ur.add(new Species("Torpe", "kicsi"));
    }
    catch(Exception e) {e.printStackTrace();}
    return ur;
    }
    
    @Produces
    @ApplicationScoped
    public HeroRepository createHeroRepository(){
        HeroRepository ur = new HeroRepository();
    return ur;
    }
    
    //---------
    
    @Produces
    @ApplicationScoped
    public BuildingRepository createBuildingRepository(){
        BuildingRepository ur = new BuildingRepository();
    return ur;
    }
    
    @Produces
    @ApplicationScoped
    public NaturalAssetRepository createNaturalAssetRepository(){
        NaturalAssetRepository ur = new NaturalAssetRepository();
    return ur;
    }
    
}
