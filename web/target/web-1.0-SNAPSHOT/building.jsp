<%-- 
    Document   : building
    Created on : 2019. szept. 24., 18:42:30
    Author     : javaee
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Birodalom adatai:</h1>
        <h3>Nev: ${currentEmpire.name}<br>
        Leiras:  ${currentEmpire.getDescription()} <br></h3>
        
        
        <c:forEach var="population" items="${currentEmpire.getPopulation()}" >
            Nepesseg: ${population.getQuantity()} <br>
        </c:forEach>
            
            Raktarkeszlet: <br>    
        <c:forEach var="warehouse" items="${currentEmpire.getWarehouse()}" >
            ${warehouse.getAsset().getName()} : ${warehouse.getQuantity()} <br>
        </c:forEach>
            
        Osszes elerheto nyersanyag: <br>    
        <c:forEach var="product" items="${currentEmpire.getProduce()}" >
            ${product.getAsset().getName()} : ${product.getQuantity()} <br>
        </c:forEach>
                   
       
        <h1>Epulet kezeles</h1>
        <form method ="post" action="BuildingServlet">    
        <c:forEach var="bdng" items="${buildings}">
            <br> ${bdng.name} <input type="submit" value="BUILD" <br>
        </c:forEach>
        </form>
    </body>
</html>
