/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.oe.javaee.adatok;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author javaee
 */
public class SpeciesRepository {
    
    
    public static final SpeciesRepository instance = new SpeciesRepository();
    
    private List<Species> species = new ArrayList();
    
    public SpeciesRepository(){
        species.add(new Species("Ember", "Okos"));
        species.add(new Species("Torpe", "Kicsi"));
    }

    public List<Species> getSpecies() {
        return species;
    }

    
    
    
}
