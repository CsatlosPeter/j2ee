/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.oe.javaee.adatok;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author javaee
 */
public class BuildingRepository {
    public static final BuildingRepository instance = new BuildingRepository();
    
    private List<Building> buildings = new ArrayList();
    
    public BuildingRepository(){
        buildings.add(new Building("Lakohaz", "Tobb ember elhet a birodalomban", 10));
        buildings.add(new Building("Szenbanya", "Szenet lehet banyaszni", 14));
        buildings.add(new Building("Kobanya", "Kovet lehet banyaszni", 12));
        buildings.add(new Building("Erdeszhaz", "Fat lehet termelni", 15));
    }

    public List<Building> getBuildings() {
        return buildings;
    }

    public void setBuildings(List<Building> buildings) {
        this.buildings = buildings;
    }
    
}
