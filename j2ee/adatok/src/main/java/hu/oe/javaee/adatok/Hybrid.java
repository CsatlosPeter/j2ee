/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.oe.javaee.adatok;

/**...
 *
 * @author javaee
 */
public class Hybrid {
    private Species species;
    private Integer percent;

    public Hybrid(Species species, Integer percent) {
        this.species = species;
        this.percent = percent;
    }

    public Hybrid() {
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public Integer getPercent() {
        return percent;
    }

    public void setPercent(Integer percent) {
        this.percent = percent;
    }
    
     
}
