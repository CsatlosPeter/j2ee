/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.oe.javaee.adatok;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author javaee
 */
public class Hero {
    private String name, description;
    private List<Hybrid> hybrid = new ArrayList<>();
    private List<Quality> qualities = new ArrayList<>();

    public Hero(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Hero() {
    }
      
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Hybrid> getHybrid() {
        return hybrid;
    }

    public void setHybrid(List<Hybrid> hybrid) {
        this.hybrid = hybrid;
    }

    public List<Quality> getQualities() {
        return qualities;
    }

    public void setQualities(List<Quality> qualities) {
        this.qualities = qualities;
    }
    
}
