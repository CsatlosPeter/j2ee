/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.oe.javaee.adatok;

import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author javaee
 */
@ApplicationScoped
public class UserRepository {
    
    
    private List<User> users = new ArrayList();
    
    public UserRepository(){
        users.add(new User("a", "a", true));
    }
    
    public void Registration(String pName, String pPassword) throws RegistrationException{
        for(User u: users){
            if(u.getName().equals(pName))
                throw new RegistrationException();
        }
        User tmpUser = new User(pName, pPassword, false);
        users.add(tmpUser);
        
        //asdsegwegseggsg
    }
    
    public User Login(String pName, String pPassword) throws LoginException{
        for(User u: users){
            if(u.getName().equals(pName) && u.getPassword().equals(pPassword))
                return u;
        }
        throw new LoginException();
    }
    
}
