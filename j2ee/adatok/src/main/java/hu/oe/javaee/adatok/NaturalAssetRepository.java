/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.oe.javaee.adatok;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author javaee
 */
public class NaturalAssetRepository {
    
    public static final NaturalAssetRepository instance = new NaturalAssetRepository();
    
    private List<NaturalAsset> naturalAssets = new ArrayList();
    
    public NaturalAssetRepository(){
        naturalAssets.add(new NaturalAsset("fa", "Epitkezeshez"));
        naturalAssets.add(new NaturalAsset("ko", "Erossebb anyag"));
        naturalAssets.add(new NaturalAsset("szen", "Futoanyag"));
    }

    public List<NaturalAsset> getNaturalAssets() {
        return naturalAssets;
    }

    public void setNaturalAssets(List<NaturalAsset> naturalAssets) {
        this.naturalAssets = naturalAssets;
    }
    
    
    
    
}
