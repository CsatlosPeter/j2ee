/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.oe.javaee.adatok;

/**
 *
 * @author javaee
 */
public class NaturalAsset {
    private String name, description;

    public NaturalAsset(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public NaturalAsset() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
