/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.oe.javaee.adatok;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author javaee
 */
public class User {
    private String name, password;
    private boolean admin;
    private List<Hero> hero = new ArrayList<>();
    
    private List<Empire> empire = new ArrayList<>();

    public User(String name, String password, boolean admin) {
        this.name = name;
        this.password = password;
        this.admin = admin;
        
        empire.add(new Empire("First test empire", "The first test empire", 1));
        empire.add(new Empire("Second test empire", "The second test empire", 1));
        hero.add(new Hero("Test hero 1", "The first test hero"));
    }

    public User() {
    }
    
    

      
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public List<Hero> getHero() {
        return hero;
    }

    public void setHero(List<Hero> hero) {
        this.hero = hero;
    }

    public List<Empire> getEmpire() {
        return empire;
    }

    public void setEmpire(List<Empire> empire) {
        this.empire = empire;
    }
    
    
    
}
