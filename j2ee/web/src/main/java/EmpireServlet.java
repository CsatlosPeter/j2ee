/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import hu.oe.javaee.adatok.BuildingRepository;
import hu.oe.javaee.adatok.NaturalAsset;
import hu.oe.javaee.adatok.Empire;
import hu.oe.javaee.adatok.Hero;
import hu.oe.javaee.adatok.Hybrid;
import hu.oe.javaee.adatok.NaturalAssetRepository;
import hu.oe.javaee.adatok.Species;
import hu.oe.javaee.adatok.SpeciesRepository;
import hu.oe.javaee.adatok.Stock;
import hu.oe.javaee.adatok.User;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author javaee
 */
@WebServlet(urlPatterns = {"/EmpireServlet"})
public class EmpireServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EmpireServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EmpireServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Empire empire = new Empire(request.getParameter("name"), request.getParameter("desc"), 1);
        for(NaturalAsset na: NaturalAssetRepository.instance.getNaturalAssets())
        {
            NaturalAsset nna = new NaturalAsset(na.getName(), na.getDescription());
            empire.getProduce().add(new Stock(nna, 1 + (long) (Math.random()*(100-1))));
            
        }
        User sess = ((User)request.getSession().getAttribute("user"));
        sess.getEmpire().add(empire);
        //response.getWriter().println(".....Birodalom hozzaadva.....");
        //response.getWriter().println(sess.getEmpire().get(0).getProduce().get(0).getAsset().getName() + sess.getEmpire().get(0).getProduce().get(0).getQuantity());
        
        //response.getWriter().println(sess.getEmpire().get(0).getProduce().get(1).getAsset().getName() + sess.getEmpire().get(0).getProduce().get(1).getQuantity());
        
        //response.getWriter().println(sess.getEmpire().get(0).getProduce().get(2).getAsset().getName() + sess.getEmpire().get(0).getProduce().get(2).getQuantity());
        request.setAttribute("buildings", BuildingRepository.instance.getBuildings());

        getServletContext().getRequestDispatcher("/building.jsp").include(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
