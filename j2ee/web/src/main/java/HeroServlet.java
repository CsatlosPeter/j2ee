import hu.oe.javaee.adatok.Hero;
import hu.oe.javaee.adatok.Hybrid;
import hu.oe.javaee.adatok.RegistrationException;
import hu.oe.javaee.adatok.Species;
import hu.oe.javaee.adatok.SpeciesRepository;
import hu.oe.javaee.adatok.User;
import hu.oe.javaee.adatok.UserRepository;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author javaee
 */
@WebServlet(name ="HeroServlet", urlPatterns = {"/newhero"})
public class HeroServlet extends HttpServlet {

    
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        Hero hero = new Hero(request.getParameter("name"), request.getParameter("desc"));
        for(Species sp: SpeciesRepository.instance.getSpecies())
        {
            Hybrid nh = new Hybrid(sp, Integer.parseInt(request.getParameter(sp.getName())));
            hero.getHybrid().add(nh);
            
        }
        User sess = ((User)request.getSession().getAttribute("user"));
        sess.getHero().add(hero);
        response.getWriter().print(hero.getName() + " siekresen létrehozva");
        
        /*for(Hybrid h:hero.getHybrid()){
           response.getWriter().print(h.getSpecies() + ": " + h.getPercent());
        }*/
        
        response.getWriter().print(".....");
        
            
    }
    //dsdsd

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
