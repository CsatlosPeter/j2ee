/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.oe.javaee.adatok;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author javaee
 */
@Entity
@Table(name ="species")
public class Species {
    
    private String name, description;
    
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private long id;
      

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    
    private List<Quality> qualities = new ArrayList<>();

    public Species(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Species() {
    }
    
    

      
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Quality> getQualities() {
        return qualities;
    }

    public void setQualities(List<Quality> qualities) {
        this.qualities = qualities;
    }

   
    
    
    
}
