/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.oe.javaee.adatok;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author javaee
 */
public class SpeciesRepository {
    private EntityManager em = Persistence.createEntityManagerFactory("HeroPu").createEntityManager();
    
    //private List<Species> species = new ArrayList();
    
        public List<Species> getSpecies() {
        //return species;
        return em.createQuery("SELECT s FROM Species s", Species.class).getResultList();
    }

    public void add(Species pValue){
        em.getTransaction().begin();
        em.persist(pValue);
        em.getTransaction().commit();
        //this.species.add(pValue);
    }
    
    
}
