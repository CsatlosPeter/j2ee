/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.oe.javaee.adatok;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author javaee
 */
public class HeroRepository {
    private EntityManager em = Persistence.createEntityManagerFactory("HeroPu").createEntityManager();
    
    //private List<Hero> hero = new ArrayList();
    
        public List<Hero> getHeroes() {
            return em.createQuery("SELECT h FROM Hero h", Hero.class).getResultList();

//return hero;
    }

    public void add(Hero pValue){
        em.getTransaction().begin();
        for(Hybrid h: pValue.getHybrid())
            em.persist(h);
        em.persist(pValue);
        em.getTransaction().commit();
        
        
        //this.hero.add(pValue);
    }
    
    public List<Hero> getByNameUser(String pName, User pUser)
    {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery(Hero.class);
        Root root = cq.from(Hero.class);
        
        cq.select(root);
        
        
        cq.where(
                cb.and(
                    cb.equal(root.get("name"), pName),
                    cb.equal(root.get("user"), pUser))
        );
        return em.createQuery(cq).getResultList();
    }
    
        public Hero getById(long id){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery(Hero.class);
        Root root = cq.from(Hero.class);
        
        cq.select(root);
        
        
        cq.where(
                cb.and(
                    cb.equal(root.get("id"), id))
        );
        return (Hero)em.createQuery(cq).getSingleResult();
    }
    
    
}
