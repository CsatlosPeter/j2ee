/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.oe.javaee.adatok;

import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author javaee
 */

public class UserRepository {
    private EntityManager em = Persistence.createEntityManagerFactory("HeroPu").createEntityManager();
    
    public void add(User pValue){
        em.getTransaction().begin();
        em.persist(pValue);
        em.getTransaction().commit();
        //this.species.add(pValue);
    }
    
    public User getByNamePassword(String pName, String pPassword)
    {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery(User.class);
        Root root = cq.from(User.class);
        cq.select(root);
        cq.where(
                cb.and(
                        cb.equal(root.get("name"), pName),
                        cb.equal(root.get("password"), pPassword)
                )
        );
        return (User)em.createQuery(cq).getSingleResult();
    }
    
    public User getByName(String pName)
    {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery(User.class);
        Root root = cq.from(User.class);
        
        cq.select(root);
        cq.where(cb.equal(root.get("name"), pName));
        return (User)em.createQuery(cq).getSingleResult();
    }
    
    public UserRepository(){
    }    
}
