/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.oe.javaee.adatok;

import java.security.Timestamp;

/**
 *
 * @author javaee
 */
public class TimeTable {
    private Hero hero;
    private Empire empire;
    private Timestamp start;
    private Timestamp stop;

    public TimeTable(Hero hero, Empire empire, Timestamp start, Timestamp stop) {
        this.hero = hero;
        this.empire = empire;
        this.start = start;
        this.stop = stop;
    }

    public TimeTable() {
    }

    public Hero getHero() {
        return hero;
    }

    public void setHero(Hero hero) {
        this.hero = hero;
    }

    public Empire getEmpire() {
        return empire;
    }

    public void setEmpire(Empire empire) {
        this.empire = empire;
    }

    public Timestamp getStart() {
        return start;
    }

    public void setStart(Timestamp start) {
        this.start = start;
    }

    public Timestamp getStop() {
        return stop;
    }

    public void setStop(Timestamp stop) {
        this.stop = stop;
    }
    
    
}
