/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.oe.javaee.adatok;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author javaee
 */
public class BuildingRepository {
    //public static final BuildingRepository instance = new BuildingRepository();
    private EntityManager em = Persistence.createEntityManagerFactory("HeroPu").createEntityManager();
    //private List<Building> buildings = new ArrayList();
    
    public List<Building> getBuildings() {
            return em.createQuery("SELECT b FROM Building b", Building.class).getResultList();
    }
    /**public BuildingRepository(){
        buildings.add(new Building("Lakohaz", "Tobb ember elhet a birodalomban", 10));
        buildings.add(new Building("Szenbanya", "Szenet lehet banyaszni", 14));
        buildings.add(new Building("Kobanya", "Kovet lehet banyaszni", 12));
        buildings.add(new Building("Erdeszhaz", "Fat lehet termelni", 15));
    }*/

        public void add(Building bValue){
        em.getTransaction().begin();
        em.persist(bValue);
        em.getTransaction().commit();
    }
    
    public Building getById(long id){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery(Building.class);
        Root root = cq.from(Building.class);
        
        cq.select(root);
        
        
        cq.where(
                cb.and(
                    cb.equal(root.get("id"), id))
        );
        return (Building)em.createQuery(cq).getSingleResult();
    }
        
     public List<Building> getOrderedByName()
     {
         CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery(Building.class);
        Root root = cq.from(Building.class);
        
        cq.select(root);
        cq.orderBy(cb.asc(root.get("name")));
        
        return em.createQuery(cq).getResultList();
     }
    /**public List<Building> getBuildings() {
        return buildings;
    }

    public void setBuildings(List<Building> buildings) {
        this.buildings = buildings;
    }*/
    
}
