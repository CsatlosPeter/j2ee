/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.oe.javaee.adatok;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author javaee
 */
@Entity
@Table(name="building")
public class Building {
    
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private long id;
    
    private String name, description;
    private List<Stock> produce = new ArrayList<>();
    
    private long buildTime;

    public Building(String name, String description, long buildTime) {
        this.name = name;
        this.description = description;
        this.buildTime = buildTime;
    }

    public Building(String name, String description) {
        this.name = name;
        this.description = description;
        this.buildTime = 10;
    }
    

    public Building() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Stock> getProduce() {
        return produce;
    }

    public void setProduce(List<Stock> produce) {
        this.produce = produce;
    }

    public long getBuildTime() {
        return buildTime;
    }

    public void setBuildTime(long buildTime) {
        this.buildTime = buildTime;
    }
    
}
