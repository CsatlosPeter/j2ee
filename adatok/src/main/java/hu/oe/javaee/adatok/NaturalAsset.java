/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.oe.javaee.adatok;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author javaee
 */
@Entity
@Table(name="naturalAsset")
public class NaturalAsset {
    
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private long id; 

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    private String name, description;

    public NaturalAsset(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public NaturalAsset() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
