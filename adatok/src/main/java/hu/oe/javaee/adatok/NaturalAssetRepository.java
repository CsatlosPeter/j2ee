/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.oe.javaee.adatok;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author javaee
 */
public class NaturalAssetRepository {
    private EntityManager em = Persistence.createEntityManagerFactory("HeroPu").createEntityManager();
    
    // private List<NaturalAsset> naturalAssets = new ArrayList();
        
    public List<NaturalAsset> getNaturalAssets() {
        //return species;
        return em.createQuery("SELECT na FROM naturalAsset na", NaturalAsset.class).getResultList();
    }
     //       public List<NaturalAsset> getNaturalAssets() {
     //   return naturalAssets;
    //}

    public void add(NaturalAsset naValue){
        em.getTransaction().begin();
        em.persist(naValue);
        em.getTransaction().commit();
        //this.species.add(pValue);
    }
    //public static final NaturalAssetRepository instance = new NaturalAssetRepository();
    
    
    //public NaturalAssetRepository(){
    //    naturalAssets.add(new NaturalAsset("fa", "Epitkezeshez"));
    //    naturalAssets.add(new NaturalAsset("ko", "Erossebb anyag"));
    //    naturalAssets.add(new NaturalAsset("szen", "Futoanyag"));
    //}

    //public void setNaturalAssets(List<NaturalAsset> naturalAssets) {
    //    this.naturalAssets = naturalAssets;
    //}
    
     public NaturalAsset getById(long id){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery(NaturalAsset.class);
        Root root = cq.from(NaturalAsset.class);
        
        cq.select(root);
        
        
        cq.where(
                cb.and(
                    cb.equal(root.get("id"), id))
        );
        return (NaturalAsset)em.createQuery(cq).getSingleResult();
    }
     
     public List<NaturalAsset> getOrderedByName()
     {
         CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery(NaturalAsset.class);
        Root root = cq.from(NaturalAsset.class);
        
        cq.select(root);
        cq.orderBy(cb.asc(root.get("name")));
        
        return em.createQuery(cq).getResultList();
     }
    
    
}
